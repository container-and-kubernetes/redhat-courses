# Redhat-Courses

  ##  Red Hat OpenShift 4 Foundations  (2days)
- Build and deploy applications with OpenShift
- Logging into an OpenShift cluster
- Build and deploy applications with odo
- Deploy applications from images
- Deploy applications from source code
- Manage resource objects
- Access and expose a database
- Transfer files in and out of containers


## Red Hat OpenShift I: Containers & Kubernetes (DO180) (2 days)

### Outline for this course
### Introducing container technology
- Describe how software can run in containers orchestrated by Red Hat OpenShift Container Platform.
### Creating containerized services
- Provision a service using container technology.
### Managing containers
- Modify pre-build container images to create and manage containerized services.
### Managing container images
- Manage the life cycle of a container image from creation to deletion.
### Creating custom container images
- Design and code a Container file to build a custom container image.
### Deploying containerized applications on OpenShift
- Deploy single container applications on OpenShift Container Platform.
### Deploying multi-container applications
- Deploy applications that are containerized using multiple container images.
- Troubleshooting containerized applications
- Troubleshoot a containerized application deployed on OpenShift.
- Comprehensive review of introduction to container, Kubernetes, and Red Hat OpenShift
- Demonstrate how to containerize a software application, test it with Podman, and deploy it on an OpenShift cluster.





## Red Hat OpenShift Administration II:Operating a Production Kubernetes Cluster (DO280) (4days)
### Outline for this course
### Describe the Red Hat OpenShift Container Platform
- Describe the architecture of the Red Hat OpenShift Container Platform (RHOCP).
### Verify the health of a cluster
- Describe OpenShift installation methods and verify the health of a newly installed cluster.
### Configure authentication and authorization
- Configure authentication with the HTPasswd identity provider and assign roles to users and groups.
- Configure application security
- Restrict permissions of applications using security context constraints and protect access credentials using secrets.
### Configure OpenShift networking for applications
- Troubleshoot OpenShift software-defined networking (SDN) and configure network policies.
- Control pod scheduling
- Control which nodes a pod runs on.
### Describe cluster updates
- Describe how to perform a cluster update.
### Manage a cluster with the web console
- Manage a Red Hat OpenShift cluster using the web console.



## Red Hat Advanced Cluster Management for Kubernetes Foundations (4 days)

##Outline for this course
### Manage a Multicluster Kubernetes Architecture
- Describe multicluster architectures and use Red Hat OpenShift Platform Plus to solve their challenges.
- Inspect Resources from Multiple Clusters Using the RHACM Web Console
- Describe and navigate the Red Hat Advanced Cluster Management for Kubernetes (RHACM) web console. - Configure role-based access control (RBAC) and search for resources across multiple clusters by using the RHACM search engine.
### Deploy and Manage Policies for Multiple Clusters with RHACM
- Deploy and manage policies in a multicluster environment by using Red Hat Advanced Cluster Management for Kubernetes (RHACM) governance.
- Install and Customize the RHACM Observability Stack
-Gain insight into the fleet of managed clusters by using Red Hat Advanced Cluster Management for Kubernetes (RHACM) observability components.
### Deploy Applications Across Multiple Clusters with RHACM
Deploy and manage applications in a multicluster environment with Red Hat Advanced Cluster Management for Kubernetes GitOps.
- Install and Configure Red Hat Quay
- Install and configure Red Hat Quay on Red Hat OpenShift Container Platform (RHOCP).
- Integrate Red Hat Quay with Red Hat OpenShift and RHACM
-Describe Red Hat Quay use cases in a multicluster environment,  and use Red Hat Advanced Cluster ### Management for Kubernetes (RHACM) to deploy applications and  control the image sources allowed in the cluster fleet.
- Install and Configure RHACS
- Install and configure Red Hat Advanced Cluster Security for Kubernetes (RHACS) and learn how it can - help organizations with security in multicluster environments.
### Multicluster Operational Security Using RHACS
- Manage the operational security of a Kubernetes cluster fleet using Red Hat Advanced Cluster Security for Kubernetes (RHACS), and integrate RHACS with external services.
